<?php
namespace emilasp\site\widgets\CircleProgress;

use emilasp\core\components\base\Widget;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class CircleProgress
 * @package emilasp\site\widgets\CircleProgress
 */
class CircleProgress extends Widget
{
    public $class = 'circle';
    public $value = 0;

    public $options = [];

    public $defaultOptions = [
        'size'                  => 30,
        //'startAngle'            => 90,
        'thickness'             => 5,
        'fill'                  => [
            'gradient' => ['green', 'red']
        ],
        'reverse'               => true,
        'animation-start-value' => 1
    ];

    public function init()
    {
        $this->options = ArrayHelper::merge($this->options, $this->defaultOptions);

        foreach ($this->options as $option => $value) {
            $this->options['data'][$option] = $value;
        }
        $this->options['id']    = $this->id;
        $this->options['class'] = $this->class;


        $this->registerAssets();
    }

    public function run()
    {
        echo Html::beginTag('div', $this->options);
        echo Html::tag('strong', '');
        echo html::endTag('div');

        /*echo '<div>
                <div
            id="circle"
            class="circle"
            data-value="0.9"
            data-size="60"
            data-thickness="20"
            data-animation-start-value="1.0"
            data-fill="{
                &quot;color&quot;: &quot;rgba(0, 0, 0, .3)&quot;,
                &quot;image&quot;: &quot;http://i.imgur.com/pT0i89v.png&quot;
            }"
            data-reverse="true"
        ><strong></strong></div></div>
        ';*/
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        CircleProgressAsset::register($view);

        $valPercent = ($this->value) ? $this->value / 100 : 0;

        $js = <<<JS
        $('#{$this->id}').circleProgress({
            value: {$valPercent},
        }).on('circle-animation-progress', function(event, progress) {
            $(this).find('strong').html(parseInt({$this->value} * progress) + '<i>%</i>');
        });
JS;
        $view->registerJs($js);
    }
}
