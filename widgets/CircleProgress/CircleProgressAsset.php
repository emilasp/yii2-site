<?php

namespace emilasp\site\widgets\CircleProgress;

use emilasp\core\components\base\AssetBundle;

/**
 * MegamenuAsset CircleProgressAsset
 * @package emilasp\site\widgets\CircleProgress
 */
class CircleProgressAsset extends AssetBundle
{
    public $jsOptions = ['position' => 1];

    public $sourcePath = __DIR__ . '/assets';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'emilasp\site\widgets\CircleProgress\CircleBowerAsset'
    ];

    public $css = ['circle'];
}
