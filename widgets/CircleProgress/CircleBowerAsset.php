<?php
namespace emilasp\site\widgets\CircleProgress;

use emilasp\core\components\base\AssetBundle;


/**
 * Class CircleBowerAsset
 * @package emilasp\site\widgets\megamenu
 */
class CircleBowerAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-circle-progress/dist';

    public $js = [
        'circle-progress'
    ];
}
