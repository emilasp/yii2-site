<?php
namespace emilasp\site\widgets\megamenu;

use emilasp\core\components\base\Widget;
use yii;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

/**
 * Class Megamenu
 * @package emilasp\site\widgets\megamenu
 */
class Megamenu extends Widget
{
    public $path = '@common/config/menu/';
    public $menu = '';

    public function init()
    {
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->renderFile(Yii::getAlias($this->path) . DIRECTORY_SEPARATOR . $this->menu . '.php');
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        MegamenuAsset::register($view);
    }
}
