<?php

namespace emilasp\site\widgets\megamenu;

use emilasp\core\components\base\AssetBundle;

/**
 * MegamenuAsset GoalListAsset
 * @package emilasp\site\widgets\megamenu
 */
class MegamenuAsset extends AssetBundle
{
    public $jsOptions = ['position' => 1];

    public $sourcePath = __DIR__ . '/assets';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'emilasp\site\widgets\megamenu\MegaAsset'
    ];

    public $css = ['menu'];
}
