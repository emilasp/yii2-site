<?php
namespace emilasp\site\widgets\megamenu;

use yii\web\AssetBundle;

/**
 * Class MegaAsset
 * @package emilasp\site\widgets\megamenu
 */
class MegaAsset extends AssetBundle
{
    public $sourcePath = '@vendor/geedmo/yamm3/yamm';

    public $css = [
        'yamm.css'
    ];

    /*public function init()
    {
        $this->css[] = 'yamm.css';
    }*/
}
