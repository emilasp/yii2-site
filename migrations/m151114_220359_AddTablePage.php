<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\site\common\models\Page;

/** ./yii migrate --migrationPath=./vendor/emilasp/yii2-site/common/migrations/
 * Class m151114_220359_AddTablePage
 */
class m151114_220359_AddTablePage extends Migration
{
    private $tableOptions = null;

    public function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
    }

    public function up()
    {
        $this->createTable('site_page', [
            'id'          => $this->primaryKey(11),
            'type'        => $this->smallInteger(1)->notNull(),
            'name'        => $this->string(255)->notNull(),
            'text'        => $this->text()->notNull(),
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_site_page_created_by',
            'site_page',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_site_page_updated_by',
            'site_page',
            'updated_by',
            'users_user',
            'id'
        );

        $indexPage = new Page();
        $indexPage->type = 1;
        $indexPage->name = 'Главная';
        $indexPage->text = 'Текст на главную';
        $indexPage->status = 1;
        $indexPage->created_by = 1;
        $indexPage->updated_by = 1;
        $indexPage->save();

        $this->addIndexPage();
    }

    public function down()
    {
        $this->dropTable('site_page');
        return true;
    }

    private function addIndexPage()
    {
        Variety::add('page_type', 'page_type_base', 'Обычная', 1, 1);
    }
}
