<?php

namespace emilasp\site\frontend\controllers;

use Yii;
use emilasp\site\common\models\Page;
use emilasp\core\components\base\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id, Page::className());

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
