<?php
namespace emilasp\site\frontend\controllers;

use emilasp\seo\frontend\behaviors\SeoFilter;
use emilasp\site\common\models\Page;
use Exception;
use HttpException;
use Yii;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use emilasp\core\components\base\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /* 'seo' => [
                 'class' => SeoFilter::className(),
                 'actions' => [
                     'index'
                 ]
             ],*/
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['site', 'page'],
                'rules' => [
                    [
                        'actions' => ['site', 'page'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            /*[
                'class' => 'yii\filters\PageCache',
                'only' => ['index'],
                'duration' => $this->module->getSetting('page_cache_duration'),
                'variations' => [
                    Yii::$app->request->get('id'),
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT MAX(updated_at) FROM site_page',
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            /*'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],*/
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $indexPageId = $this->module->getSetting('index_page');
        $model       = $this->findModel($indexPageId, Page::className());

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            return $this->render('error', [
                'name'      => $name,
                'message'   => $message,
                'exception' => $exception,
            ]);
        }
    }
}
