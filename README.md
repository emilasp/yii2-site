Модуль Site для Yii2
=============================

Модуль с основным функционалом для сайта. Страницы, статьи и т.п.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-site-new": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-site-new.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'site' => []
    ],
```
