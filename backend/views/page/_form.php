<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use emilasp\seo\backend\widgets\SeoForm\SeoForm;

/* @var $this yii\web\View */
/* @var $model emilasp\site\common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>

    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('site', 'Tab Base') ?></a></li>
            <li><a data-toggle="tab" href="#seo"><?= Yii::t('seo', 'Tab Seo') ?></a></li>
        </ul>
    </div>

    <div class="col-md-9">
        <div class="tab-content">
            <div id="base" class="tab-pane fade in active">
                <?= $form->field($model, 'type')->dropDownList($model->types) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'options' => ['rows' => 3],
                    'preset'  => 'standart',
                ]) ?>
                <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
            </div>
            <div id="seo" class="tab-pane fade">
                <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
