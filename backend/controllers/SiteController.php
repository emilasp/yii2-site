<?php
namespace emilasp\site\backend\controllers;

use emilasp\seo\frontend\behaviors\SeoFilter;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Class SiteController
 * @package emilasp\site\backend\controllers
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['error', 'captcha', 'page', 'index'],
                'rules' => [
                    [
                        'actions' => ['error', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }
}
