<?php
namespace emilasp\site\backend\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package emilasp\site\abckend\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $js       = [];
    public $css      = ['/css/site.css'];
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\emilasp\core\extensions\jsHelpers\JsHelpersAsset',
        '\emilasp\core\assets\JboxAsset',
        '\emilasp\core\assets\BaseAsset',
    ];

    public function init()
    {
        /*parent::init();

        $coreModule = \Yii::$app->getModule('site');
        if ($coreModule) {
            $this->basePath = \Yii::getAlias($coreModule->themesPath) . '/' . $coreModule->theme;
        }*/
    }
}
