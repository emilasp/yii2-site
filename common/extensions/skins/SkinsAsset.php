<?php
namespace emilasp\site\common\extensions\skins;

use emilasp\core\components\base\AssetBundle;

/**
 * Class SkinsAsset
 * @package emilasp\site\common\extensions\skins
 */
class SkinsAsset extends AssetBundle
{
    public $jsOptions=['position'=>1];

    public $sourcePath = __DIR__ . '/assets';

    public static  $theme = '';

    public static function setTheme( $view, $theme ){
        self::$theme = $theme;
        self::register($view);
    }
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $themesPath = 'themes/';
        $theme = $themesPath.self::$theme;
        $this->css = [$theme];
        parent::init();
    }


}
