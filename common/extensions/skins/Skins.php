<?php
namespace emilasp\site\common\extensions\skins;

use yii;

/**
 * Class Themes
 * @package emilasp\site\common\extensions\skins
 */
class Skins  extends \yii\base\Widget {

    const THEME_TATUHA = 'tatuha';
    const THEME_AMELIA = 'amelia';
    const THEME_CERULEAN = 'cerulean';
    const THEME_CYBORG = 'cyborg';
    const THEME_DARKLY = 'darkly';
    const THEME_FLATLY = 'flatly';
    const THEME_LUMEN = 'lumen';
    const THEME_PAPER = 'paper';
    const THEME_READABLE = 'readable';
    const THEME_SANDSTONE = 'sandstone';
    const THEME_SIMPLEX = 'simplex';
    const THEME_SLATE = 'slate';
    const THEME_SPACELAB = 'spacelab';
    const THEME_UNITED = 'united';
    const THEME_YETI = 'yeti';
    const THEME_ALPHA = 'alpha';
    const THEME_GOAL = 'goal';

    public $theme = false;

    public function init(){
        if($this->theme)  $this->registerAssets();
    }

    public function run(){}

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        SkinsAsset::setTheme($view, $this->theme);
    }
}
