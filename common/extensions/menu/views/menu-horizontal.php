<?php
use kartik\nav\NavX;
use yii\bootstrap\NavBar;

$navBarOptions = [
    'brandLabel' => '<span class="logo-amulex"></span> ',
    'options'    => [
        'class' => 'navbar navbar-inverse navbar-static-top',
        'id'    => 'topMenu',
    ],
];


NavBar::begin($navBarOptions);

foreach ($menus as $menu) {
    $_menu = [
        'activateParents' => true,
        'encodeLabels'    => false,
    ];

    if (isset($menu['options'])) {
        $_menu['options'] = $menu['options'];
    }
    if (isset($menu['items'])) {
        $_menu['items'] = $menu['items'];
    }

    //@TODO
    echo NavX::widget($_menu);
}

NavBar::end();
