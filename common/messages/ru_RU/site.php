<?php
return [
    'ID'              => 'ID',
    'Title'           => 'Title',
    'Name'            => 'Наименование',
    'Description'     => 'Описание',
    'Text'            => 'Описание',
    'Short Text'      => 'Краткое описание',
    'Image ID'        => 'Изображение',
    'User ID'         => 'Пользователь',
    'City ID'         => 'Город',
    'Manager ID'      => 'Менеджер',
    /*'Object ID' => 'Объек',
    'Object' => '',*/
    'Type'            => 'Тип',
    'Status'          => 'Статус',
    'Email'           => 'Почта',
    'Created At'      => 'Дата создания',
    'Updated At'      => 'Дата изменения',
    'Created By'      => 'Кем создан',
    'Updated By'      => 'Кем изменен',
    'Params'          => 'Параметры',
    'Manager comment' => 'Комментарий менеджера',
    'User Name'       => 'Имя',
    'Lastname'        => 'Фамилия',
    'Surname'         => 'Отчество',
    'Address'         => 'Адрес',
    'Phone'           => 'Телефон',
    'Phones'          => 'Телефоны',
    'History'         => 'История',
    'Code'            => 'Код',
    'Text Free'       => 'Текст(бесплатная доставка)',
    'Icon'            => 'Иконка',
    'Options Exclude' => 'Исключенные опции',
    'Provider'        => 'Провайдер',
    'Providers'       => 'Провайдеры',
    'Events'          => 'События',
    'Value'           => 'Значение',
    'Owner'           => 'Владелец',
    'Order'           => 'Сортировка',
    'Visible'         => 'Видимость',
    'Fio'             => 'ФИО',
    'Date'            => 'Дата',
    'Comment'         => 'Комментарий',


    'Create Page' => 'Создание страницы',
    'Page'        => 'Страница',
    'Pages'       => 'Страницы',


    'Tab Base' => 'Основное',


    'Update {modelClass}: ' => 'Редктируем {modelClass}: ',
    'Search'                => 'Поиск',
    'Reset'                 => 'Сбросить',
    'Save'                  => 'Сохранить',
    'Create'                => 'Создать',
    'Update'                => 'Редактировать',
    'Delete'                => 'Удалить',
    'Add'                   => 'Добавить',
    'Reset List'            => 'Сбросить',
    'Download'              => 'Загрузить',
    '-select-'              => '-выберите-',


    'Cities'  => 'Города',
    'Fields'  => 'Поля',
    'Options' => 'Настройки',


    'Are you sure you want to delete this item?' => 'Удалить?',
    'Please select you city'                     => 'Пожлуйста выберите ваш город',
    'Select city'                                => 'Город',
    'Select city other'                          => 'Найти свой город',
    'Contacts'                                   => 'Контакты',

    'Search city'   => 'Выбор города',
    'Search street' => 'Выбор улицы',

    'Menu'       => 'Меню',
    'Home'       => 'Домой',
    'Index page' => 'Главная страница',


    'Invalid Phone'  => 'Неверный формат номера',
    'Page not found' => 'Страница не найдена',


    'City'            => 'Город',
    'Street'          => 'Улица',
    'House'           => 'Дом',
    'Build'           => 'Строение',
    'Access'          => 'Подъезд',
    'Office'          => 'Офис/квартира',
    'Intercom'        => 'Домофон',
    'Address comment' => 'Комментарий к адресу',


    'Error info text: to admin' => 'Поздравляем, вы сломали интернет магазин.. :)',
    'Error info text: thanks'   => 'Если ошибка имеет системный характер, напишите администратору и мы сделаем для вас скидку 10% на наш товар.',


    'Send'   => 'Отправить',
    'Answer' => 'Вопрос',

    'Message' => 'Сообщение',
    'Offer'   => 'Предложение',
    'Error'   => 'Ошибка',
    'Cancel'  => 'Отменить',
];
