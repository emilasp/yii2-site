<?php
namespace emilasp\site\common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use emilasp\user\core\models\User;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\seo\common\behaviors\SeoModelBehavior;
use emilasp\variety\behaviors\VarietyModelBehavior;

/**
 * This is the model class for table "site_page".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $text
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Page extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_page';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'     => SeoModelBehavior::className(),
                'route'     => '/site/page/view'
            ],
            'variety_type'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'page_type',
            ],
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'text'], 'required'],
            [['type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],

            [['title', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'type'        => Yii::t('site', 'Type'),
            'name'        => Yii::t('site', 'Name'),
            'text'        => Yii::t('site', 'Text'),
            'status'      => Yii::t('site', 'Status'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
            'created_by'  => Yii::t('site', 'Created By'),
            'updated_by'  => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
